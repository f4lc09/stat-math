package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"

	"image/color"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
)

var nums Sortable
var N float64
var K int

func main() {
	// Reading input
	data, err := os.ReadFile("input.json")
	e(err)

	err = json.Unmarshal(data, &nums)
	e(err)
	sort.Sort(nums)

	N = float64(len(nums))
	if N == 0 {
		fmt.Println("input.json содержит пустой массив")
		return
	}

	intervalRange := nums[int(N-1)] - nums[0]
	intervalsCount := math.Floor(1 + math.Log2(N))
	K = int(math.Ceil(float64(intervalRange) / intervalsCount))
	var intervals []int
	for i := range intervalRange / K {
		i++
		intervals = append(intervals, i)
	}
	if intervalRange%K != 0 {
		intervals = append(intervals, intervals[len(intervals)-1]+1)
	}
	fmt.Println(len(nums))
	fmt.Println(K)
	fmt.Println(intervalsCount)
	// Compute frequency, cumulative frequency, relative frequency, cumulative relative frequency, and midpoints
	frequency := make(map[int]int)
	for _, num := range nums {
		i := getInterval(num)
		frequency[i]++
	}

	cumulativeFrequency := make(map[int]int)
	for _, i := range intervals {
		v := frequency[i]
		cumulativeFrequency[i] = v + cumulativeFrequency[i-1]
	}

	relativeFrequency := make(map[int]float64)
	for _, i := range intervals {
		v := frequency[i]
		relativeFrequency[i] = float64(v) / N
	}

	cumulativeRelativeFrequency := make(map[int]float64)
	for _, i := range intervals {
		v := relativeFrequency[i]
		cumulativeRelativeFrequency[i] = v + cumulativeRelativeFrequency[i-1]
	}

	mids := make(map[int]float64)
	start := nums[0]
	for _, i := range intervals {
		mids[i] = float64((start*2 + K) / 2)
		start += K
	}

	// TABLE PART
	fmt.Printf("%-4s | %-20s | %-8s | %-20s | %-11s | %-23s | %-14s\n",
		"N", "Границы интервала", "Частота", "Накопленная частота", "Частотность", "Накопленная частотность", "Среднее значение")
	fmt.Println("-------------------------------------------------------------------------------------------")
	for _, i := range intervals {
		intervalMin := nums[0] + K*(i-1)
		intervalMax := nums[0] + K*i
		fmt.Printf("%-4d | %-20s | %-8d | %-20d | %-11.2f | %-23.2f | %-14.2f\n",
			i, fmt.Sprintf("%d - %d", intervalMin, intervalMax), frequency[i], cumulativeFrequency[i], relativeFrequency[i], cumulativeRelativeFrequency[i], mids[i])
	}

	// Statistics calculations
	answer := make(map[string]string)

	// Average
	var sum float64
	for _, num := range nums {
		sum += float64(num)
	}
	sred := sum / N

	// Mode
	var modInterval int
	var max int
	for i, v := range frequency {
		if v > max {
			max = v
			modInterval = i
		}
	}
	modIntervalMaxValue := nums[0] + K*modInterval
	modIntervalMinValue := nums[0] + K*(modInterval-1)

	// intervalsCount := float64(len(intervals))
	chislitel := float64(frequency[modInterval] - frequency[modInterval-1])
	znamenatel := float64(frequency[modInterval] - frequency[modInterval-1] + frequency[modInterval] + frequency[modInterval+1])
	mod := float64(modIntervalMinValue) + (intervalsCount * chislitel / znamenatel)

	// Median
	chislitelf := intervalsCount * (0.5*N - float64(cumulativeFrequency[modInterval]))
	znamenatelf := float64(frequency[modInterval+1])
	med := float64(modIntervalMaxValue) + chislitelf/znamenatelf

	// Dispersion
	var iterDisSum float64
	for i, mid := range mids {
		iterDisSum += float64(frequency[i]) * math.Pow((mid-sred), 2)
	}
	dis := iterDisSum / (N - 1)

	// Mean square
	meanSquare := math.Sqrt(dis)

	// The coefficient of variation
	variCoef := meanSquare / sred * 100

	// Standard deviation of the arithmetic mean
	deviation := meanSquare / math.Sqrt(N)

	// Bevel measure
	bevelMeasure := sred - mod/meanSquare

	// Excess
	var iterExcSum float64
	for i, mid := range mids {
		iterExcSum += float64(frequency[i]) * math.Pow((mid-sred), 4)
	}
	znamenatelf = N * math.Pow(meanSquare, 4)
	exc := iterExcSum/znamenatelf - 3

	answer["Среднее арифметическое"] = fmt.Sprintf("%.1f", sred)
	answer["Мода"] = fmt.Sprintf("%.2f", mod)
	answer["Медиана"] = fmt.Sprintf("%.2f", med)
	answer["Размах вариации"] = strconv.Itoa(nums[len(nums)-1] - nums[0])
	answer["Дисперсия"] = fmt.Sprintf("%.2f", dis)
	answer["Среднее квадратичное отклонение"] = strconv.FormatFloat(meanSquare, 'f', 2, 64)
	answer["Коэффициент вариации"] = fmt.Sprintf("%2.2f%s", variCoef, "%")
	answer["Стандартная ошибка среднего арифметического"] = fmt.Sprintf("%.2f", deviation)
	answer["Мера скошенности"] = fmt.Sprintf("%.2f", bevelMeasure)
	answer["Эксцесс"] = fmt.Sprintf("%.2f", exc)

	fmt.Println()
	for k, v := range answer {
		fmt.Printf("%s: %s\n", k, v)
	}

	// Plotting graphs
	p := plot.New()

	p.Title.Text = "Полигон распределения частот"
	p.X.Label.Text = "Интервалы"
	p.Y.Label.Text = "Частота"

	points := make(plotter.XYs, len(intervals))
	for j, i := range intervals {
		points[j].X = mids[i]
		points[j].Y = float64(frequency[i])
	}

	line, err := plotter.NewLine(points)
	e(err)
	line.LineStyle.Width = vg.Points(1)
	line.LineStyle.Color = plotter.DefaultLineStyle.Color

	p.Add(line)
	p.Save(4*vg.Inch, 4*vg.Inch, "frequency_polygon.png")

	h := plot.New()

	h.Title.Text = "Гистограмма распределения"
	h.X.Label.Text = "Интервалы"
	h.Y.Label.Text = "Частота"

	bars := make(plotter.Values, len(intervals))
	for j, i := range intervals {
		bars[j] = float64(frequency[i])
	}

	barChart, err := plotter.NewBarChart(bars, vg.Points(20))
	e(err)
	barChart.LineStyle.Width = vg.Length(0)
	barChart.Color = color.RGBA{0, 0, 255, 255}

	h.Add(barChart)
	h.Save(4*vg.Inch, 4*vg.Inch, "frequency_histogram.png")

	fmt.Printf("\nПостроенные графики записаны в файлы: frequency_histogram.png, frequency_polygon.png\n\n")

	fmt.Println("Нажмите Enter, чтобы закрыть программу...")
	bufio.NewReader(os.Stdin).ReadBytes('\n')
}
