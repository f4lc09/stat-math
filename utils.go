package main

import (
	"errors"
	"fmt"
	"os"
)

type Sortable []int

func (a Sortable) Len() int           { return len(a) }
func (a Sortable) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a Sortable) Less(i, j int) bool { return a[i] < a[j] }

// handles errors
func e(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

// returns an interval by num
func getInterval(a int) int {
	num := nums[0]
	if a > nums[int(N-1)]+K {
		e(errors.New("a < num+K"))
	}
	for i := 1; ; i++ {
		if a >= num && a < num+K {
			return i
		}
		num += K
	}
}
